from urllib.request import urlopen
from bs4 import BeautifulSoup
import csv
import os

with open("imported_mdf_map.kml", 'r', encoding="utf8") as f:
    soup = BeautifulSoup(f, features="xml")
i=1
for placemark in soup.find_all('Placemark'):
    item_id = str(i)
    name = placemark.find('name').string.strip()

with open("soup.txt", "w", encoding="utf8") as text_file:
    text_file.write(soup.string)

data = BeautifulSoup(link, 'xml')

table_name = "councils"
sql = ""
out_file = "add_councils_sql.txt"
i=1
with open('Vic CouncilsShiresAlpine Resorts (READ NOTES) - Copy.kml', 'r', encoding="utf8") as f:
    soup = BeautifulSoup(f, 'xml')

    for placemark in soup.find_all('Placemark'):
        item_id = str(i)

        name = placemark.find('name').string.strip()

        try:   
            description = placemark.find('Data', {"name":"description"}).find('value').string.translate(str.maketrans({
                                                                                        "\\": r"\\",
                                                                                        "'":  r"\'",
                                                                                        '"':  r'\"',
                                                                                        ",":  r"\,"})).replace('"', "'").replace("\\", "").replace("“", "'").replace("”", "'")
            lines = description.split('\n')
            print(lines[0])
            new_lines = []
            for line in lines:
                words =  [ word if 'http' not in word else "<a href='{0}'>{1}</a>".format(word, word.replace("http://","www.").replace("https://","www.").replace("www.www.","www.")) for word in line.split(" ") ]
                line = " ".join(words)
                new_lines.append(line)
            description = "\n".join(new_lines)
        except:
            description = ''
            pass

        coordinates = placemark.find('coordinates').string.replace(" ", "").splitlines()
        coordinates.pop(0)
        polygon = ""
        for i in range(len(coordinates)):
            coordinates[i] = coordinates[i][:-2]
            polygon += str(coordinates[i].split(",")[0]) + ' ' + str(coordinates[i].split(",")[1]) + ','

        geometry = 'POLYGON((' + (polygon[:-1]) + '))'

        query =''
        try: 
            fly = placemark.find('Data', {"name":"Fly"}).find('value').string.strip()
            query += 'INSERT INTO ' + table_name + '(fly, name, description, position)' + ' VALUES ('
            # query += '"' + item_id + '",' #insert ID
            query += '"' + fly + '",'  #insert Status (UAVn)
            query += '"' + name + '",' #insert Name
            query += '"' + description + '",' #insert Description
            query += 'ST_GeomFromText("' + geometry + '", 4326));' + '\n'  #insert Position

        except:
            query += 'INSERT INTO ' + table_name + '(name, description, position)' + ' VALUES ('
            # query += '"' + item_id + '",' #insert ID
            query += '"' + name + '",' #insert Name
            query += '"' + description + '",' #insert Description
            query += 'ST_GeomFromText("' + geometry + '", 4326));' + '\n'  #insert Position
        
        sql += query 
        i+=1

with open(out_file, "w", encoding="utf8") as text_file:
    text_file.write(sql)


