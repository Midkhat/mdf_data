import hashlib
import requests

def md5Checksum(filePath,url):
    m = hashlib.md5()
    if url==None:
        with open(filePath, 'rb') as fh:
            m = hashlib.md5()
            while True:
                data = fh.read(8192)
                if not data:
                    break
                m.update(data)
            return m.hexdigest()
    else:
        r = requests.get(url)
        for data in r.iter_content(8192):
             m.update(data)
        return m.hexdigest()

# print (" checksum_local :",md5Checksum("Melbourne Drone Flyers Map 2019.kml",None))
# print (" checksum_local :",md5Checksum("Melbourne Drone Flyers Map 2019.kml",None))
# # print ("checksum_remote :",md5Checksum(None,"http://www.google.com/maps/d/kml?forcekml=1&mid=1gsb92VlD7u0EHYnkzkw-63wxTzS9Nlum"))