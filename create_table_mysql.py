import simplejson as json 

#------------------ CREATING TABLE SQL --------------------------------
def create_table(table_name, field_names, geometry_type):
    query = ''
    query += 'CREATE TABLE IF NOT EXISTS ' + table_name + '(\n'
    query += 'id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,\n'
    query += field_names['name'] + ' VARCHAR(150),\n'
    query += field_names['description'] + ' VARCHAR(500),\n'
    query += field_names['uav_n'] + ' VARCHAR(2),\n'
    query += field_names['position'] + ' ' + geometry_type + ' NOT NULL\n'
    query += ');\n'
    query += 'ALTER TABLE '+ table_name + ' MODIFY ' + field_names['position'] + ' GEOMETRY NOT NULL;\n'
    query +=  'ALTER TABLE '+ table_name + ' ADD SPATIAL INDEX(' + field_names['position'] + ');\n'
    query += 'SHOW COLUMNS FROM ' + table_name + ';'
    print (query)

#------------------ MAIN --------------------------------
## !!!!!! Indicate Table name, Field names and Geometry type!!!!!
field_names = {'name': 'name', 'position': 'position', 'description': 'description', "uav_n": "uav_n"}
table_name = 'councils'
geometry_type = 'POLYGON'  # POLLYGON or POINT
#geometry_type = 'POINTS'

create_table(table_name, field_names, geometry_type)