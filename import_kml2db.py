from urllib.request import urlopen
from bs4 import BeautifulSoup
import csv
import os
import mysql.connector

#DOWNLOADING KML FILE
################################################################################################
# URL =  "http://www.google.com/maps/d/kml?forcekml=1&mid=1gsb92VlD7u0EHYnkzkw-63wxTzS9Nlum"
# f = urlopen(URL)
# print ("downloading " , URL)
# # Open our local file for writing
# with open("imported_mdf_map.kml", "wb") as local_file:
#     local_file.write(f.read())
# print('dowloaded.')
################################################################################################

#OPEN KML FILE AND GET DATA
################################################################################################

# url_response = urllib2.urlopen(URL) ## return a http.response object 
# xml_content = url_response.read() ## read the content 


class Placemark:
  def __init__(self, placemark):
    self.name = placemark.find('name')
    self.description = placemark.find('description')
    self.fly = placemark.find_all('Data')[1].find('value').string
    # self.fly = placemark.find('fly')
    self.position = placemark.find_all('coordinates')
    # self.length = len(placemark)
    self.type = type(placemark)

  def get_name(self):
    return self.name.string.strip().translate(str.maketrans({"\\": r"\\", "'":  r"\'", '"':  r'\"', ",":  r"\,"}))

  def get_description(self):
    try:   
      descr = self.description.string.translate(str.maketrans({"\\": r"\\",
                                                                        "'":  r"\'",
                                                                        '"':  r'\"',
                                                                        ",":  r"\,"})).replace("'", '"').replace("\\", "").replace("“", '"').replace("”", '"').replace('s"', "'s")
      ### Create Links in description
      descr = str(descr)
      descr = descr.replace("Fly: 1","").replace("Fly: -1","").replace("Fly: 0","").replace("description: ","")
      lines = descr.split('<br>')
      new_lines = []
      for line in lines:
        words =  [ word if 'http' not in word else '<a href="{0}">{1}</a>'.format(word, word.replace("http://","www.").replace("https://","www.").replace("www.www.","www.")) for word in line.split(" ") ]
        line = " ".join(words)
        new_lines.append(line)
      descr = "<br>".join(new_lines)
    except:
      descr = ''
      pass
    return descr

  def get_fly(self):
    try:   
      fl = str(self.fly).strip()
      if fl == "None":
        fl = "0"
    except:
      fl = "0"
      pass
    return fl

  def get_position(self):
    result = []
    for element in self.position:
      element_list = []
      for point in element.string.replace(" ", "").splitlines()[1:]:
        point_string = str(point.split(',')[0]) + " " + str(point.split(',')[1])
        element_list.append(point_string)
      result.append(element_list)
    return result

# with open("imported_mdf_map.kml", 'r', encoding="utf8") as f:
#   soup = BeautifulSoup(f, features="xml")

#UPDATE DB FROM ONLINE KML FILE
################################################################################################

#CONNECTING TO DB
################################################################################################
mydb = mysql.connector.connect(
  host="192.163.221.184",
  user="commercetron_mdf_editor",
  passwd="*kJY5I9.Y7Ul",
  database="commercetron_mdf"
)
if (mydb):
  mycursor = mydb.cursor()
  print('connected')
else:
  print("didn't connect")

# mycursor.execute("SELECT AsText(position) FROM councils WHERE name = 'Bass Coast (C)';")
# myresult = mycursor.fetchall()
# for x in myresult:
#   print(x)
################################################################################################
URL = "http://www.google.com/maps/d/kml?forcekml=1&mid=1gsb92VlD7u0EHYnkzkw-63wxTzS9Nlum"
xml_content = urlopen(URL).read()
soup = BeautifulSoup(xml_content, features="xml")
table_folder = {'flying_spots': 'Drone Flying Spots', 'parks_vic': 'Parks VIC', 'councils': "Vic Councils", 'water_reservoirs': "Water Reservoirs"}

def update_db(table_name):
  # query = "INSERT INTO " + table_name + " (name, fly, description, position) VALUES (%s, %s, %s, %s)"
  # data = []
  
  ### TRUNCATE THE TABLE
  mycursor.execute('TRUNCATE TABLE ' + table_name + ';')
  print('table truncated')

  for folder in soup.find_all('Folder'):
      folder_name = folder.find('name').string
      if table_folder[table_name] in folder_name:
        # print('*'*20, "\n", folder_name, len (folder.find_all('Placemark')))
        for element in folder.find_all('Placemark'):
          p = Placemark(element)
          name = p.get_name()
          description = p.get_description()
          fly = p.get_fly()
          all_positions = p.get_position()  #list of list of points polygons [[x_y, x_y],[x_y, x_y]]
                                            #point [[x_y]]
          ### FLYIG SPOTS or WATER RESERVOIR ###
          if table_name == 'flying_spots' or table_name == 'water_reservoirs':
            position = "ST_GeomFromText('POINT(" + all_positions[0] + ")', 4326)"          
            try:
              iterator = mycursor.execute(query)
              mydb.commit()
              print("inserted " + name)
            except:
              # print(description)
              print("FAILED to insert values " + name)
          ### PARKS VIC ###
          ### break down multipolygons ###
          elif table_name == 'parks_vic':
            for polygon in all_positions:
              position = "ST_GeomFromText('MULTIPOLYGON(((" + ', '.join(polygon) + ")))', 4326)"
              query = "INSERT INTO " + table_name + " (name, fly, description, position) VALUES ('" + name + "', '" + fly + "', '" + description +  "', " + position + ");"
              try:          
                iterator = mycursor.execute(query)
                mydb.commit()
                print("inserted " + name)
              except:
                print("FAILED to insert values " + name)
          ### COUNCILS ###
          ### keep multiploygons ###
          elif table_name == 'councils':
            polygons_array = []
            # if name == 'Baw Baw (S)':
            print(name, " fly: ", fly)
            for polygon in all_positions:
              polygons_array.append( '((' + ', '.join(polygon) + '))')
            position = "ST_GeomFromText('MULTIPOLYGON(" + ', '.join(polygons_array) + ")', 4326)"
            query = "INSERT INTO " + table_name + " (name, fly, description, position) VALUES ('" + name + "', '" + fly + "', '" + description +  "', " + position + ");"
            try:
              iterator = mycursor.execute(query)
              mydb.commit()
              # print("inserted " + name)
            except:
              print("FAILED to insert values " + name)

update_db('councils')
# update_db('parks_vic')

mycursor.close()
