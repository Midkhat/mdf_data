import pandas as pd
import simplejson as json 

#------------------ CREATING TABLE SQL --------------------------------
# def create_table(table_name, field_names, geometry_type):
#     query = ''
#     query += 'CREATE TABLE IF NOT EXISTS ' + table_name + '(\n'
#     query += 'id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,\n'
#     query += field_names['name'] + ' VARCHAR(150),\n'
#     query += field_names['description'] + ' VARCHAR(500),\n'
#     query += field_names['position'] + ' ' + geometry_type + ' NOT NULL\n'
#     query += ');\n'
#     query += 'ALTER TABLE '+ table_name + ' MODIFY ' + field_names['position'] + ' GEOMETRY NOT NULL;\n'
#     query +=  'ALTER TABLE '+ table_name + ' ADD SPATIAL INDEX(' + field_names['position'] + ');\n'
#     query += 'SHOW COLUMNS FROM ' + table_name + ';'
#     print (query)

#------------------ POPULATE TABLE SQL --------------------------------
def populate_sql(input_file , output_file, table_name, geometry_type):
##### INSERT POLYGONS WITH COMMENTS (councils table) #####
    with open(input_file) as datafile:
        data = json.load(datafile)
    df = pd.DataFrame(data["features"])

    sql = ""
    for i in range(len(data["features"])):
        # creating fields for sql query
        item_id = str(i+1)
        name = df["properties"][i]["Name"].translate(str.maketrans({
                                                        "\\": r"\\",
                                                        "'":  r"\'",
                                                        '"':  r'\"',
                                                        ",":  r"\,"}))
        try:   
            description = df["properties"][i]["description"].replace("<br>", "").translate(str.maketrans({
                                                                                            "\\": r"\\",
                                                                                            "'":  r"\'",
                                                                                            '"':  r'\"',
                                                                                            ",":  r"\,"}))
        except:
            description = ''
            pass

        if geometry_type == 'points':
            coordinates = df["geometry"][i]['coordinates']
            point = str(coordinates[0]) + ' ' + str(coordinates[1])
            geometry = 'POINT(' + point + ')'  

        elif geometry_type == 'polygons':            
            coordinates = df["geometry"][i]['coordinates'][0]
            # some councils have coordinates pais inside extra list. Fix it
            if isinstance(coordinates[0][0], list):
                coordinates = coordinates[0]
                poly=''
                for c in coordinates:  
                    poly += str(c[0]) + ' ' + str(c[1]) + ','
                geometry = 'POLYGON((' + (poly[:-1]) + '))'  

        # set position of "coordinates" in JSON
        # check if POINT or POLYGON

        # Populating sql query. UAVn only added if exists
        query =''
        try: 
            uav_n = df["properties"][i]["UAVn"]
            query += 'INSERT INTO ' + table_name + '(uav_n, id, name, description, position)' + ' VALUES ('
            query += '"' + uav_n + '"),'  #insert Status (UAVn)
        except:
            query += 'INSERT INTO ' + table_name + '(id, name, description, position)' + ' VALUES ('
            query += '"' + item_id + '",' #insert ID
            query += '"' + name + '",' #insert Name
            query += '"' + description + '",' #insert Description
            query += 'ST_GeomFromText("' + geometry + '", 4326));' + '\n'  #insert Position

        sql += query
        print(i)

        with open(output_file, "w") as text_file:
            text_file.write(sql)


#------------------ MAIN --------------------------------
## !!!!!! Indicate Table name, Field names and Geometry type!!!!!
# field_names = {'name': 'name', 'position': 'position', 'description': 'description'}
# table_name = 'parks_vic'
# geometry_type = 'POLYGON'  # POLLYGON or POINT
# #geometry_type = 'POINTS'

# create_table(table_name, field_names, geometry_type)

# input_file = "MDF_flying_spots.json"
# output_file = "Output_flying_spots.txt"

# populate_sql(input_file , output_file, table_name, geometry_type)